# Django Admin classes for the 'store' django application.
#
# Copyright (C) 2016 Geo Sebastian <geo@purayidathil.org>. All Rights Reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY [LICENSOR] "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from django.contrib import admin
from inventory.models import *
from django.contrib.admin.actions import delete_selected

models = [Department,
          Godown,
          Supplier,
          Group,
]

class BulkDeleteMixin(object):
    """http://stackoverflow.com/questions/1471909/django-model-delete-not-triggered"""
    class SafeDeleteQuerysetWrapper(object):
        def __init__(self, wrapped_queryset):
            self.wrapped_queryset = wrapped_queryset

        def _safe_delete(self):
            for obj in self.wrapped_queryset:
                obj.delete()

        def __getattr__(self, attr):
            if attr == 'delete':
                return self._safe_delete
            else:
                return getattr(self.wrapped_queryset, attr)

        def __iter__(self):
            for obj in self.wrapped_queryset:
                yield obj

        def __getitem__(self, index):
            return self.wrapped_queryset[index]
        
        def __len__(self):
            return len(self.wrapped_queryset)

    def get_actions(self, request):
        actions = super(BulkDeleteMixin, self).get_actions(request)
        actions['delete_selected'] = (BulkDeleteMixin.action_safe_bulk_delete, 'delete_selected', "Delete selected")

        return actions

    def action_safe_bulk_delete(self, request, queryset):
        wrapped_queryset = BulkDeleteMixin.SafeDeleteQuerysetWrapper(queryset)
        return delete_selected(self, request, wrapped_queryset)

@admin.register(*models)
class DefaultAdmin(BulkDeleteMixin, admin.ModelAdmin):
    class Media:
        css = {
            'all': ('admin/css/custom.css',)
        }

    search_fields = ('name', )
    pass

@admin.register(Item)
class ItemAdmin(DefaultAdmin):
    def str_balance(self, obj):
        """Return the balance, formatted for display"""
        return grp_decimal(obj.balance(), obj.unit.decimal_places)
    str_balance.short_description = 'balance'
    
    list_display = ['name', 'group', 'subgroup', 'str_balance']
    list_filter = ('subgroup',)
    ordering = ('name',)

    def get_form(self, request, obj=None, **kwargs):
        form = super(ItemAdmin,self).get_form(request, obj, **kwargs)
        # form class is created per request by modelform_factory function
        # so it's safe to modify
        # we modify the the queryset
        #        form.base_fields['subgroup'].queryset = form.base_fields['foreign_key_field].queryset.filter(user=request.user)
        return form

@admin.register(ItemBalance)
class ItemBalanceAdmin(admin.ModelAdmin):
    list_display = ['item', 'godown', 'balance']
    list_filter = ('godown',)
    ordering = ('item','godown')

@admin.register(Unit)
class UnitAdmin(DefaultAdmin):
    list_display = ['name', 'symbol']

@admin.register(SubGroup)
class SubGroupAdmin(DefaultAdmin):
    list_display = ['name', 'group']

@admin.register(Invoice)
class InvoiceAdmin(DefaultAdmin):
    def str_amount(self, obj):
        return grp(obj.amount())
    str_amount.short_description = 'total'
    
    list_display = ['number', 'date', 'supplier', 'str_amount']
    list_filter = ('date',)
    ordering = ('-date',)
    raw_id_fields = ('supplier',)

@admin.register(PurchaseEntry)
class SubGroupAdmin(DefaultAdmin):
    list_display = ['item', 'quantity', 'invoice', 'date', ]

    def date(self, obj):
        return obj.invoice.date

@admin.register(Issue, Transfer)
class IssueGroupAdmin(DefaultAdmin):
    def str_quantity(self, obj):
        return grp_decimal(obj.quantity, obj.item.unit.decimal_places)
    str_quantity.short_description = 'quantity'

    list_display = ['item', 'date', 'source', 'destination', 'str_quantity']
