# Database models for the store django project.
#
# Copyright (C) 2016 Geo Sebastian <geo@purayidathil.org>. All Rights Reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY [LICENSOR] "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""The 'inventory' app models.
"""

import re
import os, locale
from datetime import date
from decimal import Decimal
from django.db import models
from django.db import IntegrityError, transaction
from django.core.validators import RegexValidator, MinValueValidator, MaxValueValidator, validate_slug
from django.core.exceptions import ValidationError, NON_FIELD_ERRORS
from django.db.models.query import QuerySet
from django.db.models.signals import pre_save, post_save, pre_delete, post_delete
from django.dispatch import receiver
import logging


################################# CONSTANTS etc  #################################

logger = logging.getLogger(__name__)

def log(action, obj, **kwargs):
    """Convenience log function"""
    logger.info("%d\t %s %s \"%s\" %s",id(obj), action, obj.__class__.__name__,obj, kwargs or "")

def warn(action, obj, **kwargs):
    """Convenience warn function"""
    logger.warn("%d\t%s %s \"%s\" %s",id(obj), action, obj.__class__.__name__,obj, kwargs or "")

def debug(action, obj, **kwargs):
    logger.debug("%d\t%s %s \"%s\" %s",id(obj), action, obj.__class__.__name__,obj, kwargs or "")


class Status:
    ACTIVE    = 'A'
    CANCELLED = 'C'
    CLOSED    = 'X'

    status_choice_map = {
        ACTIVE    : 'Active',
        CANCELLED : 'Cancelled',
        CLOSED    : 'Closed',
    }

    CHOICES = status_choice_map.items()

# Max decimal places allowed for a unit.
DECIMAL_STORAGE = 3

if os.name == "nt":
    locale.setlocale(locale.LC_ALL, 'enu')
else:
    locale.setlocale(locale.LC_NUMERIC, 'en_US')

# Currency
class Currency:
    EURO = 'EUR'
    INR  = 'INR'
    USD  = 'USD'

    symbols_map = {
        EURO : u'\u20ac',
        INR  : u'\u20b9',
        USD  : u'$',
    }

def sym(c):
    """Return currency symbol."""
    return Currency.symbols_map[c]
    
def grp(amount, currency=Currency.INR):
    """Group the amount using commas"""
    if amount is None:
        return None
    amt = locale.format("%.2f", amount, grouping=True)
    return currency and u"%s %s" % (amt, sym(currency)) or amt

def grp_decimal(amount, decimal_places):
    if decimal_places==0:
        fmt = "%d"
    else:
        fmt = "%%.%df" % decimal_places
    return locale.format(fmt, amount, grouping=True)

################################# DB MANAGERS #################################
# Active objects
class ActiveManager(models.Manager):
    """Returns only objects whose status is Active
    """
    def get_queryset(self):
        return super(ActiveManager, self).get_queryset().filter(deleted=False,
                                                                status=Status.ACTIVE)

# Non-deleted objects
# This will be set as the default 'objects' manager, since we don't want to see 'deleted'
# objects anywhere.
class NonDeletedManager(models.Manager):
    """Returns only objects whose status is not Deleted.
    """
    def get_queryset(self):
        return super(NonDeletedManager, self).get_queryset().exclude(deleted=True)



################################# VALIDATORS #################################

NoSpecialChars = RegexValidator(regex='^[A-Za-z0-9- ]+$',
                                message='No special characters allowed.')

# Regex validator doesn't work well with multi-line strings. Yes, Really. (Hint: ^ & $)
ascii_re = re.compile('^[ -~\t\x1f\x20]*$')
Ascii = RegexValidator(regex='^[ -~\x1f\x20]*$',
                       message="Only Ascii characters are allowed at this time")

def Ascii_Multi(value):
    if value:
        for line in value.splitlines():
            if not ascii_re.match(line):
                raise ValidationError('Only English characters are supported at this time. Please remove any special characters, currency symbols, or other language (unicode) characters.')

NoSpaces = RegexValidator(regex=u'^[^ \t\r\n\a]+$', message='Spaces are not allowed.')

phone_re = re.compile('^[+#)( 0-9]*$')
def validate_phone(value):
    if not phone_re.match(value):
        raise ValidationError('Use only these characters: 0-9, # (extension), () (grouping), + (country code)')

GTE0 = MinValueValidator(0, 'Value must be greater than or equal to 0')
GT0 = MinValueValidator(0.01, 'Value must be greater than 0')
LT4 = MaxValueValidator(DECIMAL_STORAGE, 'Maximum %d allowed' % DECIMAL_STORAGE)


################################# MODELS #################################
# Common model Ancestor
class TrackedModel(models.Model):
    """Model base class that encapsulates common tracking information such
    as created time, last modified time etc.
    """
    created_at = models.DateTimeField(auto_now_add = True)
    last_modified_at = models.DateTimeField(auto_now = True)
    status = models.CharField(max_length=1, editable=False,
                              default = Status.ACTIVE,
                              choices = Status.CHOICES)
    deleted = models.BooleanField(default=False, editable=False)

    active = ActiveManager()
    objects = NonDeletedManager()
    all_objects = models.Manager()

    def delete(self):
        """Delete a Tracked object.
        """
        if self.deleted:
            return
        
        if not self.id:
            warn('Try to delete non-existent object', self)
            return

        # Signal, since we aren't going to call the superclass function
        pre_delete.send(sender=self.__class__, instance=self)

        # Hook for child class processing
        self.process_delete()
        
        self.deleted=True
        self.full_clean()
        self.save()

        post_delete.send(sender=self.__class__, instance=self)
        log('Delete',self)

    def cancel(self):
        """Cancel a Tracked object. (Set status to 'C')
        """
        self.status = Status.CANCELLED
        self.full_clean()
        self.save()
        log('Cancel',self)
        
    def close(self):
        """Close a Tracked object. (Set status to 'X')
        """
        self.status = Status.CLOSED
        self.full_clean()
        self.save()
        log('Close',self)
        
    def activate(self):
        """Activate a Tracked object. (Set status to 'A')
        """
        # Hook for child class processing
        self.process_activate()
        
        self.deleted = False
        self.status = Status.ACTIVE
        self.full_clean()
        self.save()
        log('Activate',self)

    def process_delete(self):
        """Hook for additional processing during delete, in subclasses"""
        pass
    
    def process_activate(self):
        """Hook for additional processing during delete, in subclasses"""
        pass
    
    is_active    = property(lambda x: not x.deleted and x.status==Status.ACTIVE)
    is_closed    = property(lambda x: not x.deleted and x.status==Status.CLOSED)
    is_cancelled = property(lambda x: not x.deleted and x.status==Status.CANCELLED)

    class Meta:
        abstract = True


class NamedTrackedModel(TrackedModel):
    """Abstract parent for models that are named"""

    def process_delete(self):
        """When a 'named' object is deleted, it is renamed so that it will then be possible to
        recreate an object with identical name. This would work as long as the name field is
        plenty long.

        If there are other 'unique' fields in the model, it will not be possible to delete
        and then recreate with the same field data.
        """
        self.name = "DEL_%d:<%s>" % (self.id, self.name)

    def process_activate(self):
        """Special processing for named object restore"""
        if self.deleted and self.name.find('DEL_%d:<' % self.id) == 0:
            self.name = self.name[self.name.find('<')+1:-1]


    def save(self, *args, **kwargs):
        """Pre-procesing for named objects"""
        if not self.deleted:
            self.name = self.name.title()
        super(NamedTrackedModel, self).save(*args, **kwargs)


    class Meta:
        abstract = True


# This is a special class to simply the add/deletion of units.
class Unit(NamedTrackedModel):
    """The unit to quantify an item. m, cm, liters, pair etc"""

    name = models.CharField(max_length=56, validators=[Ascii])
    symbol = models.CharField(max_length=32, validators=[NoSpaces])
    decimal_places = models.PositiveSmallIntegerField(default=0, validators=[LT4])

    def save(self, *args, **kwargs):
        """Pre-procesing for named objects"""
        self.symbol = self.symbol.lower()
        super(Unit, self).save(*args, **kwargs)

    class Meta:
        unique_together = ('name', 'symbol')

    def __unicode__(self):
        return self.symbol


class Department(NamedTrackedModel):
    """A consumer of items form a godown"""
    name = models.CharField(max_length=128, validators=[Ascii], unique=True)

    def __unicode__(self):
        return self.name


class Godown(NamedTrackedModel):
    """Store, where items are received and kept"""
    name = models.CharField(max_length=128, validators=[Ascii], unique=True)

    @staticmethod
    def default():
        list = Godown.objects.all().order_by('id')
        if len(list)==0:
            raise NoGodownsFoundException()
        return list[0]
    
    def __unicode__(self):
        return self.name

    def item_balance(self, item):
        """Return the item balance in this godown"""
        return ItemBalance.objects.get(godown=self, item=item).balance
    
class Address:
    """Container, for address related information"""
    # Countries
    Countries = (
        ('INDIA', 'India'),
    )
    
    # States
    IN_States = (
        "Andaman and Nicobar Islands",
        "Andhra Pradesh",
        "Arunachal Pradesh",
        "Assam",
        "Bihar",
        "Chandigarh",
        "Chhattisgarh",
        "Dadra and Nagar Haveli",
        "Daman and Diu",
        "Delhi",
        "Goa",
        "Gujarat",
        "Haryana",
        "Himachal Pradesh",
        "Jammu and Kashmir",
        "Jharkhand",
        "Karnataka",
        "Kerala",
        "Lakshadweep",
        "Madhya Pradesh",
        "Maharashtra",
        "Manipur",
        "Meghalaya",
        "Mizoram",
        "Nagaland",
        "Odisha",
        "Puducherry",
        "Punjab",
        "Rajasthan",
        "Sikkim",
        "Tamil Nadu",
        "Telangana",
        "Tripura",
        "Uttar Pradesh",
        "Uttarakhand",
        "West Bengal",
    )

    IN_State_Choices = [(x,x) for x in IN_States]



class Supplier(NamedTrackedModel):
    """Selling party"""

    name = models.CharField(max_length=255, validators=[Ascii], unique=True)
    alias = models.CharField(max_length=255, validators=[Ascii], blank=True)
    tin  = models.CharField(max_length=32, validators=[NoSpecialChars], blank=True)
    address_line1 = models.CharField(max_length=255, validators=[Ascii])
    address_line2 = models.CharField(max_length=255, validators=[Ascii], blank=True)
    city = models.CharField(max_length=128, validators=[NoSpecialChars])
    state = models.CharField(max_length=128, choices=Address.IN_State_Choices)
    country = models.CharField(max_length=128, choices=Address.Countries, default=Address.Countries[0])
    pin = models.PositiveIntegerField(blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    phone = models.CharField(max_length=32, validators=[validate_phone], blank=True)
    mobile = models.CharField(max_length=32, validators=[validate_phone], blank=True)
    
    def __unicode__(self):
        return self.alias or self.name


class Taxation:
    NONE = Decimal(0)
    vat_percentages = [NONE, Decimal(14.5), Decimal(5.00)]
    VAT_CHOICES = [(x,x) for x in vat_percentages]


class Group(NamedTrackedModel):
    """A collection of sub-groups"""
    name = models.CharField(max_length=56, validators=[Ascii], unique=True)

    def __unicode__(self):
        return self.name


class SubGroup(NamedTrackedModel):
    """A collection of items"""
    name = models.CharField(max_length=56, validators=[Ascii], unique=True)
    group = models.ForeignKey(Group)

    def __unicode__(self):
        return self.name

class NoGodownsFoundException(Exception):
    def __init__(self, *args, **kwargs):
        super(NoGodownsFoundException, self).__init__('No Godowns found! Create at least one godown')

class MultipleGodownsFoundException(Exception):
    def __init__(self, *args, **kwargs):
        super(MultipleGodownsFoundException, self).__init__('Cannot update opening stock, as the item is present in multiple godowns.')

class InsufficientBalanceException(Exception):
    def __init__(self, *args, **kwargs):
        super(InsufficientBalanceException, self).__init__('This makes the item balance negative; hence not allowed.')


#ItemBalance is not a tracked model.
class ItemBalance(models.Model):
    """Internal table to keep track of item balances for efficient lookups"""
    godown = models.ForeignKey(Godown)
    item = models.ForeignKey('Item')
    balance = models.DecimalField(max_digits=12, decimal_places=DECIMAL_STORAGE, validators=[GTE0])

    class Meta:
        unique_together = ('godown','item')
    
    def __unicode__(self):
        return u"%s" % self.item

    def clean(self):
        """Custom Validations"""
        self.item.validate_decimal_places(self, 'balance')


# When an item is created, it is added to the default (first) godown. At least one godown must be
# present before an item can be created.
class Item(NamedTrackedModel):
    """Item"""
    name = models.CharField(max_length=128, validators=[Ascii], unique=True)
    unit = models.ForeignKey(Unit)
    min_stock = models.DecimalField(max_digits=16, decimal_places=DECIMAL_STORAGE,
                                    default=Decimal(0))
    tax_percentage = models.DecimalField(max_digits=5, decimal_places=2,
                                         choices=Taxation.VAT_CHOICES, default=Taxation.NONE)
    opening_stock = models.DecimalField(max_digits=16, decimal_places=DECIMAL_STORAGE,
                                        default=Decimal(0))
    opening_unit_price = models.DecimalField(max_digits=12, decimal_places=2, default=Decimal(0.00))
    subgroup = models.ForeignKey(SubGroup)

    def balance(self):
        """Get balance of this item in all godowns"""
        return sum(x.balance for x in ItemBalance.objects.filter(item=self))
    
    def group(self):
        return self.subgroup.group
    
    def validate_decimal_places(self, obj, field):
        """Validates the number has only so many max decimal places"""
        places = self.unit.decimal_places
        val = str(getattr(obj, field))
        prd =  val.find('.')

        if prd == -1:
            return
        
        # If the unit allows decimal
        if places > 0:
            debug('places=%d,val=%s' % (places,val), self)
            # A smaller number of decimal places allowed by unit, than what can be stored.
            if places < DECIMAL_STORAGE:
                if int(val[prd+1+places:]) != 0:
                    raise ValidationError({ field : '%s unit only allows upto %d decimal places precision. Value=%s' % (self.name, places, val)})
            # Else, fine.
        # whole number only
        else:
            if prd != -1 and int(val[prd+1:]) != 0:
                raise ValidationError({field : '%s %s must be specified as a whole number' % (self.name, field)})

    def clean(self):
        """Custom validation:
        1. Ensure the quantity decimal places don't exceed what the unit defines.
        2. Make sure an adjustment in opening_stock is allowed.
        """
        self.validate_decimal_places(self, 'min_stock')
        self.validate_decimal_places(self, 'opening_stock')
        
        __old = self.id and Item.all_objects.get(id=self.id) or None
        # New object or deleted object getting activated: ItemBalance will be created in post_save
        # No fear of balance going negative here.
        if not self.id or __old.deleted:
            return

        # Change in opening stock? Make sure that the item is only in one godown, and that the balance
        # will not go negative. We don't do the actual balance updation here, but in post_save.
        elif self.opening_stock != __old.opening_stock:
            debug('pre_save: opening_stock changed', self, id=self.id)
            ibset = ItemBalance.objects.filter(item=self, godown__deleted=False)
            if len(ibset) == 1:
                offset = self.opening_stock - __old.opening_stock
                if ibset[0].balance + offset < 0:
                    raise ValidationError({'opening_stock' : InsufficientBalanceException()})

            else:
                raise ValidationError(MultipleGodownsFoundException())
    
    def __unicode__(self):
        return self.name


# Handle Item opening_stock adjustments
@receiver(pre_save, sender=Item)
def item_pre_save(sender, **kwargs):
    """Pre-save hook. Save the old instance here"""
    i = kwargs['instance']
    debug('pre_save', i, id=i.id)
    if i.deleted:
        return # No-op if changes are made on a deleted item.

    fields = kwargs['update_fields']
    if fields and not 'opening_stock' in fields:
        return

    i.__old = i.id and Item.all_objects.get(id=i.id) or None


@receiver(post_save, sender=Item)
def item_post_save(sender, **kwargs):
    """For a newly created Item, add an ItemBalance entry, for the default godown.
    If the opening stock was updated, adjust it in the balance if the item is only present in one
    godown. Else, raise en exception.
    """
    i = kwargs['instance']
    debug('post_save', i, id=i.id)

    if i.deleted:
        return # No-op if changes are made on a deleted item.

    fields = kwargs['update_fields']
    if fields and not 'opening_stock' in fields:
        return

    if kwargs.get('created', False) or i.__old.deleted == True: # new / activate()
        ib = ItemBalance(godown=Godown.default(), item=i, balance=i.opening_stock)
        ib.save()
        log('New Item, opening balance = %.4f' % i.opening_stock, i)
    
    elif i.__old.opening_stock != i.opening_stock:
        offset = i.opening_stock - i.__old.opening_stock
        ib = ItemBalance.objects.filter(item=i, godown__deleted=False)[0]
        ib.balance += offset
        # ib.full_clean()  This is somehow failing at the Test_Item.test_item_balance.
        ib.save()
        log('Item balance offset with %.4f' % offset, i)


@receiver(pre_delete, sender=Item)
def item_pre_delete(sender, **kwargs):
    """Can't allow item deletion, if it is used in PEs.
    """
    i = kwargs['instance']
    debug('pre_delete', i, id=i.id)

    if i.deleted:
        return
    
    peset = i.purchaseentry_set.filter(deleted=False)
    if len(peset):
        raise Exception("Item is part of many purchases, and hence cannot be deleted")
    
    for ib in ItemBalance.objects.filter(item=i):
        ib.delete() # purge



### The following models have no 'name' attribute; it is not allowed to delete and recreate them.
class Invoice(TrackedModel):
    """An invoice"""
    number = models.CharField(max_length=56, validators=[Ascii])
    date = models.DateField()
    supplier = models.ForeignKey(Supplier)

    total_tax = models.DecimalField(max_digits=12, decimal_places=2, default=Decimal(0.0), blank=True)
    round_off = models.DecimalField(max_digits=12, decimal_places=2, default=Decimal(0.0), blank=True)

    def amount(self):
        total = sum(x.quantity * x.unit_price for x in PurchaseEntry.objects.filter(invoice=self))
        return total + self.total_tax - self.round_off

    def clean(self):
        """ Perform validations.
        1. The invoice number should be unique for the Financial Year.
        """
        y = self.date.year
        fy_start = date(self.date.month > 3 and y or y-1, 4, 1)
        fy_end = date(fy_start.year+1, 3, 31)

        objs = Invoice.all_objects.filter(date__range=(fy_start, fy_end), number=self.number)
        if self.id:
            objs = objs.exclude(id=self.id)
        if len(objs):
            raise ValidationError({'number' : 'Invoice number must be unique in the Financial Year'})

        
    def __unicode__(self):
        return self.number


class PurchaseEntry(TrackedModel):
    """An item purchase entry in an invoice"""
    item = models.ForeignKey(Item)
    quantity = models.DecimalField(max_digits=12, decimal_places=DECIMAL_STORAGE, validators=[GT0])
    unit_price = models.DecimalField(max_digits=12, decimal_places=2, validators=[GTE0])
    invoice = models.ForeignKey(Invoice)

    def __unicode__(self):
        return u"[%s] %s" % (self.invoice, self.item)

    def clean(self):
        """Custom Validations"""
        if self.deleted:
            return # No-op if changes are made on a deleted PE

        self.item.validate_decimal_places(self, 'quantity')

        # Validate item balance doesn't go negative
        if self.id:
            __old = PurchaseEntry.all_objects.get(id=self.id)

            # Was deleted earlier
            if __old.deleted:
                return

            # Change in quantity? Make sure the ItemBalance won't go negative.
            elif __old.quantity > self.quantity:
                main_bal = ItemBalance.objects.get(godown=Godown.default(), item=self.item)
                offset = self.quantity - __old.quantity
                if main_bal.balance + offset < 0:
                    raise ValidationError({'quantity' : InsufficientBalanceException()})


@receiver(pre_save, sender=PurchaseEntry)
def purchaseentry_pre_save(sender, **kwargs):
    """Pre-save hook. Save the old instance here"""
    obj = kwargs['instance']

    # If the object is getting deleted, do nothing here; pre_delete takes of it.
    if obj.deleted:
        return

    fields = kwargs['update_fields']
    if fields and not 'quantity' in fields:
        return

    obj.__old = obj.id and PurchaseEntry.all_objects.get(id=obj.id) or None


@receiver(post_save, sender=PurchaseEntry)
def purchaseentry_post_saves(sender, **kwargs):
    """For a new purchase entry, add the quantity to the item in the default godown.
       For edits in quantity, modify the quantity in the default godown.
    """
    pe = kwargs['instance']
    debug('post_save', pe, id=pe.id)

    if pe.deleted:
        warn('save called on deleted object: ', pe)
        return # sanity check: no-op for deleted things

    fields = kwargs['update_fields']
    if fields and not 'quantity' in fields:
        return

    # For a new PE, the item balance in the main godown is incremented.
    # Adjustments in PE quantity is only allowed from the default godown.
    main_bal = ItemBalance.objects.get(godown=Godown.default(), item=pe.item)
    if kwargs.get('created', False) or pe.__old.deleted == True:
        main_bal.balance += pe.quantity
        main_bal.full_clean()
        main_bal.save()
        log('New PE',pe,quantity=pe.quantity)
    
    elif pe.__old.quantity != pe.quantity:
        offset = pe.quantity - pe.__old.quantity
        main_bal.balance += offset
        main_bal.full_clean()
        main_bal.save()
        log('PE quantity offset with %.4f' % offset, pe)


@receiver(pre_delete, sender=PurchaseEntry)
def purchaseentry_pre_delete(sender, **kwargs):
    """When a purchase entry is to be deleted, remove its balance entry entirely.
    """
    pe = kwargs['instance']
    debug('pre_delete', pe, id=pe.id)
    if pe.deleted:
        return
    
    try:
        ib = ItemBalance.objects.get(item=pe.item, godown=Godown.default())
        ib.balance -= pe.quantity
        ib.full_clean()
        ib.save()
    except ItemBalance.DoesNotExist:
        pass


class Issue(TrackedModel):
    """Issue of item from a Godown to a Department"""
    date = models.DateField()
    item = models.ForeignKey(Item)
    quantity = models.DecimalField(max_digits=12, decimal_places=DECIMAL_STORAGE, validators=[GT0])
    source = models.ForeignKey(Godown)
    destination = models.ForeignKey(Department)
    
    def __unicode__(self):
        return u"[%s] %s -> %s" % (self.created_at, self.item, self.destination)

    def clean(self):
        """Custom Validations"""
        if self.deleted:
            return # No-op if changes are made on a deleted item.

        self.item.validate_decimal_places(self, 'quantity')

        # Verify item balance doesn't go negative (if quantity is adjusted)
        __old = self.id and Issue.all_objects.get(id=self.id) or None

        ib = ItemBalance.objects.get(godown=self.source, item=self.item)
        balance = ib.balance
        if not self.id or __old.deleted:
            balance -= self.quantity
        elif self.quantity > __old.quantity:
            balance -= self.quantity - __old.quantity
        else:
            return

        if balance < 0:
            raise ValidationError({'quantity' : InsufficientBalanceException()})


@receiver(post_save, sender=Issue)
def issue_pre_save(sender, **kwargs):
    """Pre-save hook. Save the old instance here"""
    i = kwargs['instance']
    if i.deleted:
        return

    fields = kwargs['update_fields']
    if fields and not 'quantity' in fields:
        return

    i.__old = i.id and Issue.all_objects.get(id=i.id) or None

@receiver(post_save, sender=Issue)
def issue_post_save(sender, **kwargs):
    """For a new issue, add the quantity to the item in the default godown.
       For edits in quantity, modify the quantity in the default godown.
    """
    i = kwargs['instance']
    debug('post_save', i, id=i.id)

    if i.deleted:
        return # sanity check: no-op for deleted things

    fields = kwargs['update_fields']
    if fields and not 'quantity' in fields:
        return

    main_bal = ItemBalance.objects.get(godown=i.source, item=i.item)
    if kwargs.get('created', False) or i.__old.deleted == True:
        main_bal.balance -= i.quantity
        main_bal.full_clean()
        main_bal.save()
        log('New Issue', i, quantity=i.quantity)
    
    elif i.__old.quantity != i.quantity:
        offset = i.quantity - i.__old.quantity
        main_bal.balance -= offset
        main_bal.full_clean()
        main_bal.save()
        log('Issue quantity offset with %.4f' % offset, i)


@receiver(pre_delete, sender=Issue)
def issue_pre_delete(sender, **kwargs):
    """When an Issue is to be deleted, remove its balance entry entirely.
    """
    i = kwargs['instance']
    debug('pre_delete', i, id=i.id)

    if i.deleted:
        return
    
    try:
        ib = ItemBalance.objects.get(item=i.item, godown=i.source)
        ib.balance += i.quantity
        ib.full_clean()
        ib.save()
    except ItemBalance.DoesNotExist:
        pass


class Transfer(TrackedModel):
    """Transfer of item betwen Godowns"""
    date = models.DateField()
    item = models.ForeignKey(Item)
    quantity = models.DecimalField(max_digits=12, decimal_places=DECIMAL_STORAGE, validators=[GT0])
    source = models.ForeignKey(Godown, related_name='out_transfers')
    destination = models.ForeignKey(Godown, related_name='in_transfers')

    def __unicode__(self):
        return u"[%s] %s -> %s" % (self.date, self.item, self.destination)

    def clean(self):
        """Custom Validations"""
        self.item.validate_decimal_places(self, 'quantity')


# NOTES
# 1. Re: null=True usage (https://docs.djangoproject.com/en/1.9/ref/models/fields/#django.db.models.Field.null)
#   Avoid using null on string-based fields such as CharField and
#   TextField because empty string values will always be stored as
#   empty strings, not as NULL. If a string-based field has null=True,
#   that means it has two possible values for "no data": NULL, and the
#   empty string. In most cases, it's redundant to have two possible
#   values for "no data;" the Django convention is to use the empty
#   string, not NULL.
# 2. Since deletion is only a state change, the uniqueness constraint remains.
#   However, since we replaced the default manager with 'NonDeleted',
#   django validation will not detect the duplicate, but DB will complain as the
#   uniqueness is enforced at database level.
#   Changing the unique constraint from field to (field, status) also doesn't help; we
#   run into the same problem with more than one delete.
# 3. Overriding the delete() to change a flag has made things complex and confusing.
#   Need to ensure that the pre_save/post_save functions don't do anything when called
#   due to the save() call in the delete function.
