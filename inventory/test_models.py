from datetime import tzinfo, timedelta, datetime
import time
import unittest
import logging

from random import Random
from django.test import TestCase, TransactionTestCase
from inventory.models import *
from django.core.exceptions import ValidationError, NON_FIELD_ERRORS

logger = logging.getLogger(__name__)
ZERO = timedelta(0)
RAND = Random()

class UTC(tzinfo):
    def utcoffset(self, dt):
        return ZERO
    def tzname(self, dt):
        return "UTC"
    def dst(self, dt):
        return ZERO

start_time = datetime.now(UTC())

def sav(x):
    x.save()
    return x

def cl_sav(x):
    x.full_clean()
    x.save()
    return x

# Common functions (TrackedModel)
def test_tracked_status(test, obj):
    """Do Status changes, and verify the tracked fields"""
    ct = obj.created_at
    lmt = obj.last_modified_at
    cls = obj.__class__

    nm = None
    try:
        nm = obj.name
    except AttributeError:
        pass

    test.assertTrue(obj.created_at > start_time)
    test.assertTrue(obj.last_modified_at >= lmt) # >= to account for blazing fastness.

    # cancel
    obj.cancel()
    obj = cls.all_objects.get(id=obj.id)
    test.assertEquals(obj.status, Status.CANCELLED)
    test.assertTrue(obj.last_modified_at >= lmt)

    # activate
    obj.activate()
    obj = cls.all_objects.get(id=obj.id)
    test.assertEquals(obj.deleted, False)
    test.assertEquals(obj.status, Status.ACTIVE)
    test.assertTrue(obj.last_modified_at >= lmt)

    # double delete
    obj.delete()
    obj.delete()

    # double activate
    obj.activate()
    obj.activate()

    # check if the name is same
    if nm:
        test.assertEquals(nm, obj.name)

    # close
    obj.close()
    obj = cls.all_objects.get(id=obj.id)
    test.assertEquals(obj.status, Status.CLOSED)
    test.assertTrue(obj.last_modified_at >= lmt)

    # delete
    obj.delete()
    obj = cls.all_objects.get(id=obj.id)
    test.assertEquals(obj.deleted, True)
    test.assertTrue(obj.last_modified_at >= lmt)

    # activate again
    obj.activate()
    obj = cls.all_objects.get(id=obj.id)
    test.assertEquals(obj.deleted, False)
    test.assertEquals(obj.status, Status.ACTIVE)
    test.assertTrue(obj.last_modified_at >= lmt)

    if nm:
        test.assertEquals(nm, obj.name) # Should be restored if renamed.

    # Timestamp should update for manual changes to fields too.
    prev = obj.last_modified_at
    obj.name = "TMPTMP"
    obj.save()
    time.sleep(1)
    test.assertTrue(obj.last_modified_at > prev)

    # created time should remain the same throughout
    test.assertEquals(ct, obj.created_at)


def test_custom_querysets(test, obj):
    """Make sure the custom querysets work as intended.
    Create at least two objects of the model before calling this method
    """

    cls = obj.__class__
    cls.objects.all().update(status=Status.CLOSED)
    test.assertTrue(len(cls.objects.all()) > 1)
    test.assertTrue(len(cls.active.all()) == 0)
    test.assertTrue(len(cls.all_objects.all()) > 1)

    # Instance delete
    x = cls.objects.get(id=obj.id)
    x.delete()
    x = cls.all_objects.get(id=obj.id)  # Still there
    with test.assertRaises(cls.DoesNotExist):    
        x = cls.objects.get(id=obj.id)
    with test.assertRaises(cls.DoesNotExist):    
        x = cls.active.get(id=obj.id)

    # Re-activate
    x.activate()
    x = cls.objects.get(id=obj.id)
    x = cls.all_objects.get(id=obj.id)
    x = cls.objects.get(id=obj.id)

    # Cancel
    x.cancel()
    test.assertTrue(len(cls.objects.all()) >= 1)
    test.assertTrue(len(cls.active.all()) == 0)
    test.assertTrue(len(cls.all_objects.all()) > 1)
    
    # Bulk delete taketh them all
    cls.objects.all().delete()
    with test.assertRaises(cls.DoesNotExist):
        x = cls.objects.get(id=obj.id)
    with test.assertRaises(cls.DoesNotExist):
        x = cls.all_objects.get(id=obj.id)
    with test.assertRaises(cls.DoesNotExist):
        x = cls.active.get(id=obj.id)


class Test_Unit(TransactionTestCase):

    def test_status(self):
        """Check status changes work fine"""
        x = Unit(name="Pair", symbol="Pair")
        x.save()
        test_tracked_status(self, x)

        a = Unit(name="Gravity", symbol="g")
        a.delete() # no-op
        a.activate() # save
        self.assertIsNotNone(a.id)

        b = Unit(name="INR", symbol=u'\u20b9')
        b.cancel()
        self.assertIsNotNone(b.id)

        c = Unit(name="Alpha", symbol='A')
        c.close()
        self.assertIsNotNone(c.id)
        
        
    def test_custom_querysets(self):
        """Verify the querysets work as intended"""
        x = Unit(name="Pair", symbol="Pair")
        y = Unit(name="Liters", symbol="Lts")
        x.save()
        y.save()
        test_custom_querysets(self, x)

    def test_create(self):
        """Verify creation, uniqueness, validators etc"""

        x = Unit(name="Length", symbol="Meters")
        x.save()
        Unit(name="Carets", symbol="^").save()
        Unit(name="Lumens", symbol="Lumens").save()

        # unique_together only
        Unit(name="Poetry", symbol="Meters").full_clean()
        Unit(name="Length", symbol="Kms").full_clean()

        # OK to have some unicode characters in symbol
        Unit(name="Euro", symbol=u"\u20ac").full_clean()

        # but keep the name in ascii.
        with self.assertRaises(ValidationError):
            Unit(name=u"Euro\u20ac", symbol=u"\u20ac").full_clean()

        # case insensitive
        with self.assertRaises(ValidationError):
            Unit(name=u"LUMENS", symbol=u"LUMENS").full_clean()

        with self.assertRaises(ValidationError):
            Unit(name=u"lumens", symbol=u"Lumens").full_clean()

        with self.assertRaises(ValidationError):
            Unit(name="Length", symbol=u"Meters").full_clean()

        with self.assertRaises(ValidationError):
            Unit(name="", symbol="Test").full_clean()

        with self.assertRaises(ValidationError):
            Unit(name="Test", symbol="").full_clean()

        with self.assertRaises(ValidationError):
            Unit(name="Test", symbol="Yeah\r").full_clean()

        with self.assertRaises(ValidationError):
            Unit(name="Test", symbol="Yeah\t").full_clean()

        # no spaces
        with self.assertRaises(ValidationError):
            Unit(name="Test", symbol="Y e a h").full_clean()

        # Only max 3 decimal places
        with self.assertRaises(ValidationError):
            Unit(name='Meters', symbol='m', decimal_places=4).full_clean()


    def test_recreate(self):
        """Recreate object with same information"""
        x = Unit(name="Len", symbol="Meters")
        x.save()

        x.delete()
        y = Unit(name="Len", symbol="Meters")
        y.full_clean()
        y.save()

        y.delete()
        z = Unit(name="Len", symbol="Meters")
        z.full_clean()
        z.save()

        with self.assertRaises(ValidationError):
            y.activate()

        cheat = Unit(name='X', symbol='Y')
        cheat.save()
        cheat.symbol = 'Meters'
        cheat.save()
        cheat.name = 'Len'
        with self.assertRaises(ValidationError):
            cheat.full_clean()


class Test_Department(TransactionTestCase):

    def test_status(self):
        """Check status changes work fine"""
        x = Department(name="Intermix")
        x.save()
        test_tracked_status(self, x)

        a = Department(name="Finishing")
        a.delete() # no-op
        a.activate() # save
        self.assertIsNotNone(a.id)

        b = Department(name="Intermix 2")
        b.cancel()
        self.assertIsNotNone(b.id)

        c = Department(name="Intermix 3")
        c.close()
        self.assertIsNotNone(c.id)

    def test_custom_querysets(self):
        """Verify the querysets work as intended"""
        x = Department(name="Store")
        y = Department(name="Press-XIV")
        x.save()
        y.save()
        test_custom_querysets(self, x)

    def test_create(self):
        """Verify creation, uniqueness, validators etc"""
        d = Department(name="Office")
        d.save()

        with self.assertRaises(ValidationError):
            Department(name="Office").full_clean()
        
        with self.assertRaises(ValidationError):
            Department(name="office").full_clean()
        
        with self.assertRaises(ValidationError):
            Department(name="").full_clean()
        
        with self.assertRaises(ValidationError):
            Department(name=u"2000\u20ac").full_clean()
        
        with self.assertRaises(ValidationError):
            Department(name="<a\tb>").full_clean()

        with self.assertRaises(ValidationError):
            Department(name="""A Ridiculously big department name
            I mean, who names these departments?
            We can't allow names this big, for sure
            But if it passes the validator,
            Who am I to say otherwise?
            """).full_clean()


    def test_recreate(self):
        """Recreate object with same information"""

        d = Department(name="Finishing")
        d.save()

        d.delete()
        y = Department(name="Finishing")
        y.full_clean()
        y.save()

        y.delete()
        z = Department(name="Finishing")
        z.full_clean()
        z.save()
        
        with self.assertRaises(ValidationError):
            y.activate()


class Test_Godown(TransactionTestCase):

    def test_status(self):
        """Check status changes work fine"""
        x = Godown(name="Main Store")
        x.save()
        test_tracked_status(self, x)

        a = Godown(name="Chemicals Room")
        a.delete() # no-op
        a.activate() # save
        self.assertIsNotNone(a.id)

        b = Godown(name="Electrical Store")
        b.cancel()
        self.assertIsNotNone(b.id)

        c = Godown(name="IT Store")
        c.close()
        self.assertIsNotNone(c.id)

    def test_custom_querysets(self):
        """Verify the querysets work as intended"""
        x = Godown(name="GPP Godown")
        y = Godown(name="GRP Godown")
        x.save()
        y.save()        
        test_custom_querysets(self, x)


    def test_create(self):
        """Verify creation, uniqueness, validators etc"""
        d = Godown(name="Office")
        d.save()

        with self.assertRaises(ValidationError):
            Godown(name="Office").full_clean()
        
        with self.assertRaises(ValidationError):
            Godown(name="office").full_clean()
        
        with self.assertRaises(ValidationError):
            Godown(name="").full_clean()
        
        with self.assertRaises(ValidationError):
            Godown(name=u"2000\u20ac").full_clean()
        
        with self.assertRaises(ValidationError):
            Godown(name="<a\tb>").full_clean()

        
    def test_recreate(self):
        """Recreate object with same information"""

        d = Godown(name="MAIN")
        d.save()

        d.delete()
        y = Godown(name="MAIN")
        y.full_clean()
        y.save()

        y.delete()
        z = Godown(name="MAIN")
        z.full_clean()
        z.save()

        with self.assertRaises(ValidationError):
            y.activate()


def mksup(name, alias='', tin=None):
    """Makes up a dummy Supplier object"""
    if tin is None:
        tin = RAND.randrange(1)
    return Supplier(name=name,
                    alias=alias,
                    tin=tin,
                    address_line1 = 'Purayidathil, House # (1)',
                    city = 'Kottayam Dt',
                    state='Kerala',
                    country='INDIA',
                    pin='686513',
                    email='gs.' + str(tin) + '@nosuch.domain',
    )


class Test_Supplier(TransactionTestCase):

    def test_status(self):
        """Check status changes work fine"""
        x = mksup('A', 'The Loved One', 1)
        x.save()
        test_tracked_status(self, x)

        a = mksup('ABC', 'ALphabets', 1)
        a.delete() # no-op
        a.activate() # save
        self.assertIsNotNone(a.id)

        b = mksup('BCD')
        b.cancel()
        self.assertIsNotNone(b.id)

        c = mksup('ACD', 'The Loved One')
        c.close()
        self.assertIsNotNone(c.id)

        
    def test_custom_querysets(self):
        """Verify the querysets work as intended"""
        x = mksup('Kurt', 'Supplier-1', 128)
        y = mksup('Eddie', 'Supplier-2', 129)
        x.save()
        y.save()
        test_custom_querysets(self, x)
        
    def test_createe(self):
        """Verify creation, uniqueness, validators etc"""
        x = mksup('John', "", 90)
        x.mobile="+1 (518) 253 0154"
        x.phone="9880000001"
        x.save()

        with self.assertRaises(ValidationError):
            mksup('John', "JL", 1).full_clean()

        # case
        with self.assertRaises(ValidationError):
            mksup('john', "JL", 1).full_clean()

        with self.assertRaises(ValidationError):
            mksup('', "JL", 1).full_clean()

        with self.assertRaises(ValidationError):
            mksup(u'Frank', u"\u20ac").full_clean()

        with self.assertRaises(ValidationError):
            mksup(u'Frank \u20ac', "").full_clean()


    def test_recreate(self):
        """Recreate object with same information"""
        x = mksup('Paul', 'MC')
        x.save()
        
        x.delete()
        y = mksup('Paul', '1')
        y.full_clean()
        y.save()

        y.delete()
        z = mksup('Paul', '22')
        z.full_clean()
        z.save()

        with self.assertRaises(ValidationError):
            y.activate()


class Test_Item(TransactionTestCase):

    def setUp(self):
        # At least one Godown must be created, before creating an item
        Godown(name='Main Store').save()
        
    def test_status(self):
        """Check status changes work fine"""
        u = Unit(name='cm', symbol='cm')
        u.save()
        g = Group(name='MECH')
        g.save()
        s = SubGroup(name='Bolts', group=g)
        s.save()
        i = Item(name='Screw 1"', unit=u, tax_percentage=Decimal("14.5"), subgroup=s)
        i.save()
        test_tracked_status(self, i)

        a = Item(name='Allen Bolt 3mm"', unit=u, tax_percentage=Decimal(14.5), subgroup=s)
        a.delete() # no-op
        a.activate() # save
        self.assertIsNotNone(a.id)

        b = Item(name='Allen Bolt 6mm"', unit=u, tax_percentage=Decimal("5"), subgroup=s)
        b.cancel()
        self.assertIsNotNone(b.id)

        c = Item(name='Allen Bolt 9mm"', unit=u, tax_percentage=Decimal(5), subgroup=s)
        c.close()
        self.assertIsNotNone(c.id)
        

    def test_custom_querysets(self):
        """Verify the querysets work as intended"""
        u = Unit(name='cm', symbol='cm')
        u.save()
        g = Group(name='MECH')
        g.save()
        s = SubGroup(name='Bolts', group=g)
        s.save()
        i1 = Item(name='Screw 1"', unit=u, tax_percentage=Decimal(5), subgroup=s)
        i2 = Item(name='Screw 2"', unit=u, tax_percentage=Decimal(5), subgroup=s)
        i1.save()
        i2.save()

        test_custom_querysets(self, i2)

    def test_create(self):
        """Verify creation, uniqueness, validators etc"""

        u = Unit(name='cm', symbol='cms')
        u.save()
        g = Group(name='MECH')
        g.save()
        s = SubGroup(name='Bolts', group=g)
        s.save()

        # delete pre-existing godowns
        Godown.all_objects.all().delete()
        i1 = Item(name='Spanner 14x15"', unit=u, tax_percentage=Decimal("14.50"), subgroup=s)
        i2 = Item(name='Screw driver small"', unit=u, tax_percentage=Decimal("5"), subgroup=s)
        with self.assertRaises(NoGodownsFoundException):
            i1.save()

        Godown(name='main').save()
        i1.save()
        i2.save()

        with self.assertRaises(ValidationError):
            Item(name='Spanner 14x15"', unit=u, tax_percentage=Decimal(1), subgroup=s).full_clean()

        # case
        with self.assertRaises(ValidationError):
            Item(name='SPANNER 14x15"', unit=u, tax_percentage=Decimal(1), subgroup=s).full_clean()

        with self.assertRaises(ValidationError):
            Item(name='', unit=u, tax_percentage=Decimal(1), subgroup=s).full_clean()

        with self.assertRaises(ValidationError):
            Item(name='Spanner\t20x21"', unit=u, tax_percentage=Decimal(1), subgroup=s).full_clean()

        with self.assertRaises(ValidationError):
            Item(name=u'Spanner 14x15\u20ac"', unit=u, tax_percentage=Decimal(14.5), subgroup=s).full_clean()

        # tax choice
        with self.assertRaises(ValidationError):
            Item(name=u'Spanner 15x16"', unit=u, tax_percentage=Decimal(4), subgroup=s).full_clean()

        i2.min_stock=32000
        i2.save()


    def test_recreate(self):
        """Verify unique names"""
        u = Unit(name='cm', symbol='cm')
        u.save()
        g = Group(name='MECH')
        g.save()
        s = SubGroup(name='Bolts', group=g)
        s.save()
        i = Item(name='Screw 1"', unit=u, tax_percentage=Decimal("14.5"), subgroup=s)
        i.save()

        i.delete()
        j = Item(name='Screw 1"', unit=u, tax_percentage=Decimal("14.5"), subgroup=s)
        j.full_clean()
        j.save()

        j.delete()
        k = Item(name='Screw 1"', unit=u, tax_percentage=Decimal("14.5"), subgroup=s)
        k.full_clean()
        k.save()

        with self.assertRaises(ValidationError):
            j.activate()


    def test_opening_stock(self):
        """Test validation of item unit decimal places"""
        x = Unit(name='Number', symbol='no', decimal_places=0)
        x.save()
        g = Group(name='MECH')
        g.save()
        s = SubGroup(name='Tools', group=g)
        s.save()

        Item(name='Spanner', unit=x, opening_stock=Decimal('10'), subgroup=s).full_clean()
        Item(name='Spanner', unit=x, opening_stock=Decimal('10.'), subgroup=s).full_clean()
        Item(name='Spanner', unit=x, opening_stock=Decimal(10.), subgroup=s).full_clean()
        Item(name='Spanner', unit=x, opening_stock=Decimal('10.00'), subgroup=s).full_clean()

        with self.assertRaises(ValidationError):
            Item(name='Spanner', unit=x, opening_stock=Decimal(22/7.0), subgroup=s).full_clean()

        with self.assertRaises(ValidationError):
            Item(name='Spanner', unit=x, opening_stock=Decimal('10.1'), subgroup=s).full_clean()
        
        y = Unit(name='Milli Liters', symbol='ml', decimal_places=3)
        y.save()
        t = SubGroup(name='Liquids', group=g)
        t.save()

        Item(name='Oil', unit=y, subgroup=t).full_clean()
        Item(name='Oil', unit=y, subgroup=t, opening_stock=Decimal('10')).full_clean()
        Item(name='Oil', unit=y, subgroup=t, opening_stock=Decimal('10.1')).full_clean()
        Item(name='Oil', unit=y, subgroup=t, opening_stock=Decimal('10.453')).full_clean()

        with self.assertRaises(ValidationError):
            Item(name='Oil', unit=y, subgroup=t, opening_stock=Decimal('10.4530')).full_clean()
    
    def test_min_stock(self):
        """Test validation of item unit decimal places"""
        x = Unit(name='Number', symbol='no', decimal_places=0)
        x.save()
        g = Group(name='MECH')
        g.save()
        s = SubGroup(name='Tools', group=g)
        s.save()

        Item(name='Spanner', unit=x, min_stock=Decimal('10'), subgroup=s).full_clean()
        Item(name='Spanner', unit=x, min_stock=Decimal('10.'), subgroup=s).full_clean()
        Item(name='Spanner', unit=x, min_stock=Decimal(10.), subgroup=s).full_clean()
        Item(name='Spanner', unit=x, min_stock=Decimal('10.000'), subgroup=s).full_clean()

        with self.assertRaises(ValidationError):
            Item(name='Spanner', unit=x, min_stock=Decimal('10.1'), subgroup=s).full_clean()
        
        with self.assertRaises(ValidationError):
            Item(name='Spanner', unit=x, min_stock=Decimal(22/7.0), subgroup=s).full_clean()

        y = Unit(name='Milli Liters', symbol='ml', decimal_places=3)
        y.save()
        t = SubGroup(name='Liquids', group=g)
        t.save()
        
        Item(name='Oil', unit=y, subgroup=t).full_clean()
        Item(name='Oil', unit=y, subgroup=t, min_stock=Decimal('10')).full_clean()
        Item(name='Oil', unit=y, subgroup=t, min_stock=Decimal('10.1')).full_clean()
        Item(name='Oil', unit=y, subgroup=t, min_stock=Decimal('10.453')).full_clean()

        with self.assertRaises(ValidationError):
            Item(name='Oil', unit=y, subgroup=t, min_stock=Decimal('10.4530')).full_clean()


    def test_item_balance(self):
        """Test item balance after doing operations.
        """
        x = sav( Unit(name='Number', symbol='no', decimal_places=0))
        g = sav( Group(name='MECH'))
        s = sav( SubGroup(name='Tools', group=g))

        i1 = cl_sav(Item(name='Spanner', unit=x, min_stock=Decimal('10'), subgroup=s))
        sub = Godown(name='Chemical Room')
        sub.save()

        main = Godown.objects.all()[0]
        self.assertEquals(main.item_balance(i1), 0)
        i1.opening_stock = 100
        i1.save(update_fields=['tax_percentage'])
        self.assertEquals(main.item_balance(i1), 0)
        i1.save()
        self.assertEquals(main.item_balance(i1), 100)
        i1.activate()
        self.assertEquals(main.item_balance(i1), 100)
        i1.min_stock = 10
        self.assertEquals(main.item_balance(i1), 100)
        i1.opening_stock = 101
        i1.save(update_fields=['opening_stock'])
        self.assertEquals(main.item_balance(i1), 101)

        debug('failing-test-start', self)
        i1.opening_stock = 100
        i1.close()
        debug('failing-test-end', self)
        self.assertEquals(main.item_balance(i1), 100)
        
        i1.delete()
        with self.assertRaises(ItemBalance.DoesNotExist):
            self.assertEquals(main.item_balance(i1), 100)

        i1.delete()
        i1.opening_stock = 90
        i1.close()
        i1.activate()
        i1.activate()
        self.assertEquals(main.item_balance(i1), 90)

        y = sav( Unit(name='Mill Liters', symbol='no', decimal_places=3))
        t = sav( SubGroup(name='Liquids', group=g))

        i2 = cl_sav(Item(name='Oil', unit=y, min_stock=Decimal('10.45'), opening_stock=Decimal('0.5'), subgroup=t))
        self.assertEquals(main.item_balance(i2), Decimal('0.5'))
        i2.opening_stock = Decimal('1.59')
        sav (i2)
        self.assertEquals(main.item_balance(i2), Decimal('1.59'))

        # Can't update once the item is present in multiple godowns


        
class Test_Group(TransactionTestCase):
    
    def test_status(self):
        """Check status changes work fine"""
        g = Group(name='Mechanical')
        g.save()
        test_tracked_status(self, g)

        a = Group(name='Electrical')
        a.delete() # no-op
        a.activate() # save
        self.assertIsNotNone(a.id)

        b = Group(name='IT')
        b.cancel()
        self.assertIsNotNone(b.id)

        c = Group(name='Misc')
        c.close()
        self.assertIsNotNone(c.id)
        
        
    def test_custom_queryset(self):
        """Verify the querysets work as intended"""
        g1 = Group(name='Mech')
        g2 = Group(name='Electrical')
        g1.save()
        g2.save()

        test_custom_querysets(self, g2)

    def test_create(self):
        """Verify creation, uniqueness, validators etc"""

        g1 = Group(name='Elementary, truly')
        g2 = Group(name='<M>')
        g1.save()
        g2.save()

        with self.assertRaises(ValidationError):
            Group(name='ELEMENTARY, truly').full_clean()

        with self.assertRaises(ValidationError):
            Group(name='elementary, Truly').full_clean()

        with self.assertRaises(ValidationError):
            Group(name='<M>').full_clean()

        with self.assertRaises(ValidationError):
            Group(name='').full_clean()

        with self.assertRaises(ValidationError):
            Group(name='MECH\t20x21"').full_clean()

        with self.assertRaises(ValidationError):
            Group(name=u'GRP \u20ac"').full_clean()


        # Delete
        g1.delete()
        g2.delete()

    def test_recreate(self):
        """Verify unique names"""
        g = Group(name='INTERMIX')
        g.save()

        g.delete()
        y = Group(name='INTERMIX')
        y.full_clean()
        y.save()

        y.delete()
        z = Group(name='INTERMIX')
        z.full_clean()
        z.save()


class Test_SubGroup(TransactionTestCase):
    
    def test_status(self):
        """Check status changes work fine"""
        x = Group(name='MECH')
        x.save()
        g = SubGroup(name='Screws', group=x)
        g.save()
        test_tracked_status(self, g)

        a = SubGroup(name='Nuts', group=x)
        a.delete() # no-op
        a.activate() # save
        self.assertIsNotNone(a.id)

        b = SubGroup(name='Bolts', group=x)
        b.cancel()
        self.assertIsNotNone(b.id)

        c = SubGroup(name='Washers', group=x)
        c.close()
        self.assertIsNotNone(c.id)


    def test_custom_querysets(self):
        """Verify the querysets work as intended"""
        x = Group(name='MECH')
        x.save()
        
        g1 = SubGroup(name='Mech', group=x)
        g2 = SubGroup(name='Electrical', group=x)
        g1.save()
        g2.save()

        test_custom_querysets(self, g2)

    def test_create(self):
        """Verify creation, uniqueness, validators etc"""

        u = Unit(name='cm', symbol='cm')
        u.save()

        x = Group(name='MECH')
        x.save()

        g1 = SubGroup(name='E', group=x)
        g2 = SubGroup(name='<M>', group=x)
        g1.save()
        g2.save()

        # Can't create without a group
        g3 = SubGroup(name='LOL')
        with self.assertRaises(IntegrityError):
            g3.save()

        # Naming rules
        with self.assertRaises(ValidationError):
            SubGroup(name='<M>').full_clean()

        # case
        with self.assertRaises(ValidationError):
            SubGroup(name='e').full_clean()

        with self.assertRaises(ValidationError):
            SubGroup(name='').full_clean()

        with self.assertRaises(ValidationError):
            SubGroup(name='MECH\t20x21"').full_clean()

        with self.assertRaises(ValidationError):
            SubGroup(name=u'GRP \u20ac"').full_clean()


    def test_recreate(self):
        """Verify unique names"""
        p = Group(name='Electrical')
        p.save()
        g = SubGroup(name='INTERMIX', group=p)
        g.save()

        g.delete()
        s = SubGroup(name='INTERMIX', group=p)
        s.full_clean()
        s.save()

        s.delete()
        t = SubGroup(name='INTERMIX', group=p)
        t.full_clean()
        t.save()

        with self.assertRaises(ValidationError):
            s.activate()


class Test_Invoice(TransactionTestCase):
    
    def test_status(self):
        """Check status changes work fine"""
        sup = mksup('Louie', 'CK')
        sup.save()
        i = Invoice(number='E109', date=date.today(), supplier=sup)
        i.save()
        test_tracked_status(self, i)

        a = Invoice(number='E0', date=date.today(), supplier=sup)
        a.delete() # no-op
        a.activate() # save
        self.assertIsNotNone(a.id)

        b = Invoice(number='0', date=date.today(), supplier=sup)
        b.cancel()
        self.assertIsNotNone(b.id)

        c = Invoice(number='Washers', date=date.today(), supplier=sup)
        c.close()
        self.assertIsNotNone(c.id)


    def test_custom_querysets(self):
        """Verify the querysets work as intended"""
        sup = mksup('A', 'B')
        sup.save()
        i1 = Invoice(number='Mech', date=date.today(), supplier=sup)
        i2 = Invoice(number='Electrical', date=date.today(), supplier=sup)
        i1.save()
        i2.save()

        test_custom_querysets(self, i2)

    def test_create(self):
        """Verify creation, uniqueness, validators etc"""
        sup = mksup('A', 'B')
        sup.save()
        i1 = Invoice(number='<Mech>', date=date.today(), supplier=sup)
        i2 = Invoice(number='Electrical', date=date(2017, 03, 01), supplier=sup)
        i1.save()
        i2.save()

        # Can't create without a supplier
        i3 = Invoice(number='LOL', date=date.today())
        with self.assertRaises(IntegrityError):
            i3.save()

        # Naming rules
        with self.assertRaises(ValidationError):
            Invoice(number='<Mech>', date=date.today(), supplier=sup).full_clean()
        tomorrow = date.today() + timedelta(days=1)
        with self.assertRaises(ValidationError):
            Invoice(number='<Mech>', date=tomorrow, supplier=sup).full_clean()

        # next fy
        Invoice(number='Electrical', date=date(2017, 04, 01), supplier=sup).full_clean()
        Invoice(number='Electrical', date=date(2018, 05, 01), supplier=sup).full_clean()
        
        # case
        with self.assertRaises(ValidationError):
            Invoice(number='<MECH>', date=date.today(), supplier=sup).full_clean()

        with self.assertRaises(ValidationError):
            Invoice(number='', date=date.today(), supplier=sup).full_clean()

        with self.assertRaises(ValidationError):
            Invoice(number='MECH\t20x21"', date=date.today(), supplier=sup).full_clean()

        with self.assertRaises(ValidationError):
            Invoice(number=u'GRP \u20ac"', date=date.today(), supplier=sup).full_clean()


    def test_recreate(self):
        """Verify unique names"""
        sup = mksup('Default')
        sup.save()
        r = Invoice(number='E-001', date=date.today(), supplier=sup)
        r.save()

        r.delete()
        s = Invoice(number='E-001', date=date.today(), supplier=sup)
        with self.assertRaises(ValidationError):
            s.full_clean()

        r.activate()

class Test_PurchaseEntry(TransactionTestCase):
    
    def setUp(self):
        # At least one Godown must be created, before creating an item
        Godown(name='Main Store').save()
        
    def test_status(self):
        """Check status changes work fine"""
        sup = mksup('CSK', 'Traders')
        sup.save()
        i = Invoice(number='E109', date=date.today(), supplier=sup)
        i.save()

        y = Unit(name='Milli Liters', symbol='ml', decimal_places=3)
        y.save()
        g = Group(name='Mechanical')
        g.save()
        t = SubGroup(name='Liquids', group=g)
        t.save()
        
        i1 = Item(name='Oil', unit=y, subgroup=t)
        i2 = Item(name='Grease', unit=y, subgroup=t)
        i1.save()
        i2.save()

        p1 = PurchaseEntry(item=i1, quantity=Decimal(1), unit_price=Decimal(10), invoice=i)
        p1.save()
        test_tracked_status(self, p1)

        p2 = PurchaseEntry(item=i2, quantity=Decimal('1.01'), unit_price=Decimal('10.25'), invoice=i)
        p2.delete() # no-op
        p2.activate() # save
        self.assertIsNotNone(p2.id)

        p3 = PurchaseEntry(item=i2, quantity=Decimal('1.01'), unit_price=Decimal(10.25), invoice=i)
        p3.cancel()
        self.assertIsNotNone(p3.id)

        p4 = PurchaseEntry(item=i2, quantity=Decimal('1.01'), unit_price=Decimal(10.25), invoice=i)
        p4.close()
        self.assertIsNotNone(p4.id)


    def test_custom_querysets(self):
        """Verify the querysets work as intended"""
        sup = mksup('CSK', 'Traders')
        sup.save()
        i = Invoice(number='E109', date=date.today(), supplier=sup)
        i.save()

        y = Unit(name='Milli Liters', symbol='ml', decimal_places=3)
        y.save()
        g = Group(name='Mechanical')
        g.save()
        t = SubGroup(name='Liquids', group=g)
        t.save()
        
        i1 = Item(name='Oil', unit=y, subgroup=t)
        i2 = Item(name='Grease', unit=y, subgroup=t)
        i1.save()
        i2.save()

        p1 = PurchaseEntry(item=i1, quantity=Decimal(1), unit_price=Decimal(10), invoice=i)
        p1.save()
        p2 = PurchaseEntry(item=i2, quantity=Decimal('1.01'), unit_price=Decimal('10.25'), invoice=i)
        p2.save()
        
        test_custom_querysets(self, p2)

    def test_create(self):
        """Verify creation, uniqueness, validators etc"""
        sup = mksup('CSK', 'Traders')
        sup.save()
        i = Invoice(number='E109', date=date.today(), supplier=sup)
        i.save()

        y = Unit(name='Milli Liters', symbol='ml', decimal_places=3)
        y.save()
        g = Group(name='Mechanical')
        g.save()
        t = SubGroup(name='Liquids', group=g)
        t.save()
        
        i1 = Item(name='Oil', unit=y, subgroup=t)
        i2 = Item(name='Grease', unit=y, subgroup=t)

        i1.save()
        i2.save()

        # Can't create without an invoice
        p1 = PurchaseEntry(item=i1, quantity=Decimal(1), unit_price=Decimal(10))
        with self.assertRaises(IntegrityError):
            p1.save()
        p1.invoice = i
        p1.save()
        
        # Quantity
        with self.assertRaises(ValidationError):
            PurchaseEntry(item=i1, quantity=Decimal('0'), unit_price=Decimal('0'), invoice=i).full_clean()

        # quantity decimal places
        PurchaseEntry(item=i1, quantity=Decimal('10'), unit_price=Decimal(1), invoice=i).full_clean()
        PurchaseEntry(item=i1, quantity=Decimal('0.101'), unit_price=Decimal('101.98'), invoice=i).full_clean()
        PurchaseEntry(item=i2, quantity=Decimal('9999.999'), unit_price=Decimal('1.10'), invoice=i).full_clean()

        with self.assertRaises(ValidationError):
            PurchaseEntry(item=i1, quantity=Decimal('10'), unit_price=Decimal('191.1001'), invoice=i).full_clean()
        
        z = Unit(name='Liters', symbol='L', decimal_places=0)
        z.save()
        i3 = Item(name='Water', unit=z, subgroup=t)
        i3.save()

        PurchaseEntry(item=i3, quantity=Decimal(9999), unit_price=Decimal('1.10'), invoice=i).full_clean()
        with self.assertRaises(ValidationError):
            PurchaseEntry(item=i3, quantity=Decimal('0.1'), unit_price=Decimal('191.1001'), invoice=i).full_clean()
        
        
    def test_recreate(self):
        """Verify unique names"""
        sup = sav( mksup('CSK', 'Traders'))
        i = sav( Invoice(number='E109', date=date.today(), supplier=sup))

        y = sav( Unit(name='Milli Liters', symbol='ml', decimal_places=3))
        g = sav( Group(name='Mechanical'))
        t = sav( SubGroup(name='Liquids', group=g))
        
        i1 = sav( Item(name='Oil', unit=y, subgroup=t))
        i2 = sav( Item(name='Grease', unit=y, subgroup=t))

        p1 = PurchaseEntry(item=i1, quantity=Decimal(1), unit_price=Decimal(10), invoice=i)
        p1.save()
        p1.delete()

        PurchaseEntry(item=i1, quantity=Decimal(1), unit_price=Decimal(10), invoice=i).save()
        p1.activate()


    def test_item_balance(self):
        """Verify item balance, on PurchaseEntry operations"""
        
        sup = sav( mksup('CSK', 'Traders'))
        i = sav( Invoice(number='E109', date=date.today(), supplier=sup))

        y = sav( Unit(name='Milli Liters', symbol='ml', decimal_places=3))
        g = sav( Group(name='Mechanical'))
        t = sav( SubGroup(name='Liquids', group=g))
        
        i1 = sav( Item(name='Oil', unit=y, subgroup=t, opening_stock=Decimal('8.445')))

        u2 = sav( Unit(name='Deci Meter', symbol='dm', decimal_places='1'))
        i2 = sav( Item(name='Wire', unit=u2, opening_stock=Decimal('1.1'), subgroup=t))

        main = Godown.default()
        
        p1 = sav( PurchaseEntry(item=i1, quantity=Decimal('1.555'), unit_price=Decimal(10), invoice=i))

        self.assertEquals(main.item_balance(i1), 10)
        p1.delete()
        self.assertEquals(main.item_balance(i1), Decimal('8.445'))

        p1.activate()
        self.assertEquals(main.item_balance(i1), 10)
        p1.quantity=20
        p1.save()
        self.assertEquals(main.item_balance(i1), Decimal('28.445'))

        # Try deleting i1.
        with self.assertRaises(Exception):
            i1.delete()

        # Delete after deleting all the PEs
        for pe in i1.purchaseentry_set.all():
            pe.delete()
        i1.delete()


        p2 = sav( PurchaseEntry(item=i2, quantity=Decimal('1.6'), unit_price=Decimal(10), invoice=i))
        self.assertEquals(main.item_balance(i2), Decimal('2.7'))
        p2.quantity=Decimal('0.8')
        p2.save()
        self.assertEquals(main.item_balance(i2), Decimal('1.9'))
        

class Test_Issue(TransactionTestCase):
    
    def setUp(self):
        # At least one Godown must be created, before creating an item
        Godown(name='Main Store').save()
        
    def test_status(self):
        """Check status changes work fine"""

        sup = sav( mksup('CSK', 'Traders'))
        i = sav( Invoice(number='E109', date=date.today(), supplier=sup))

        y = sav( Unit(name='Milli Liters', symbol='ml', decimal_places=3))
        g = sav( Group(name='Mechanical'))
        t = sav( SubGroup(name='Liquids', group=g))
        
        i1 = sav( Item(name='Oil', unit=y, subgroup=t, opening_stock=Decimal('8.445')))

        u2 = sav( Unit(name='Deci Meter', symbol='dm', decimal_places='1'))
        i2 = sav( Item(name='Wire', unit=u2, opening_stock=Decimal('1.1'), subgroup=t))

        main = Godown.default()
        dept = sav (Department(name='Intermix'))
        
        p1 = sav( PurchaseEntry(item=i1, quantity=Decimal('1.555'), unit_price=Decimal(10), invoice=i))
        iss1 = sav (Issue(date=date.today(), item=i1, quantity=10, source=main, destination=dept))
        test_tracked_status(self, iss1)

        p2 = Issue(date=date.today(), item=i2, quantity=Decimal('.1'), source=main, destination=dept)
        p2.delete() # no-op
        p2.activate() # save
        self.assertIsNotNone(p2.id)

        p3 = Issue(date=date.today(), item=i2, quantity=Decimal('.1'), source=main, destination=dept)
        p3.cancel()
        self.assertIsNotNone(p3.id)

        p4 = Issue(date=date.today(), item=i2, quantity=Decimal('.1'), source=main, destination=dept)
        p4.close()
        self.assertIsNotNone(p4.id)


    def test_custom_querysets(self):
        """Verify the querysets work as intended"""
        sup = sav( mksup('CSK', 'Traders'))
        i = sav( Invoice(number='E109', date=date.today(), supplier=sup))

        y = sav( Unit(name='Milli Liters', symbol='ml', decimal_places=3))
        g = sav( Group(name='Mechanical'))
        t = sav( SubGroup(name='Liquids', group=g))
        
        i1 = sav( Item(name='Oil', unit=y, subgroup=t, opening_stock=Decimal('8.445')))

        u2 = sav( Unit(name='Deci Meter', symbol='dm', decimal_places='1'))
        i2 = sav( Item(name='Wire', unit=u2, opening_stock=Decimal('1.1'), subgroup=t))

        main = Godown.default()
        dept = sav (Department(name='Intermix'))
        
        p1 = sav( PurchaseEntry(item=i1, quantity=Decimal('1.555'), unit_price=Decimal(10), invoice=i))
        iss1 = sav (Issue(date=date.today(), item=i1, quantity=10, source=main, destination=dept))
        iss2 = sav (Issue(date=date.today(), item=i2, quantity=Decimal('0.1'), source=main, destination=dept))
        
        test_custom_querysets(self, iss2)

    def test_create(self):
        """Verify creation, uniqueness, validators etc"""
        
        sup = sav( mksup('CSK', 'Traders'))
        i = sav( Invoice(number='E109', date=date.today(), supplier=sup))

        y = sav( Unit(name='Milli Liters', symbol='ml', decimal_places=3))
        g = sav( Group(name='Mechanical'))
        t = sav( SubGroup(name='Liquids', group=g))
        
        i1 = sav( Item(name='Oil', unit=y, subgroup=t, opening_stock=Decimal('8.445')))

        u2 = sav( Unit(name='Deci Meter', symbol='dm', decimal_places='1'))
        i2 = sav( Item(name='Wire', unit=u2, opening_stock=Decimal('1.1'), subgroup=t))

        main = Godown.default()
        dept = sav (Department(name='Intermix'))

        iss1 = cl_sav( Issue(date=date.today(), item=i1, quantity=Decimal('1'), source=main, destination=dept))
        
        # Can't create without sufficient quantity remaining
        iss2 = Issue(date=date.today(), item=i1, quantity=Decimal('10'), source=main, destination=dept)
        with self.assertRaises(ValidationError):
            iss2.full_clean()
        self.assertIsNone(iss2.id)

        iss3 = Issue(date=date.today(), item=i1, quantity=Decimal('0.0'), source=main, destination=dept)
        with self.assertRaises(ValidationError):
            iss3.full_clean()

        # quantity decimal places
        Issue(date=date.today(), item=i1, quantity=Decimal('1.101'), source=main, destination=dept).full_clean()
        Issue(date=date.today(), item=i2, quantity=Decimal('1.1'), source=main, destination=dept).full_clean()
        
        with self.assertRaises(ValidationError):
            Issue(date=date.today(), item=i2, quantity=Decimal('0.01'), source=main, destination=dept).full_clean()

    
    def test_recreate(self):
        """Verify unique names"""
        sup = sav( mksup('CSK', 'Traders'))
        i = sav( Invoice(number='E109', date=date.today(), supplier=sup))

        y = sav( Unit(name='Milli Liters', symbol='ml', decimal_places=3))
        g = sav( Group(name='Mechanical'))
        t = sav( SubGroup(name='Liquids', group=g))
        
        i1 = sav( Item(name='Oil', unit=y, subgroup=t, opening_stock=Decimal('10')))
        i2 = sav( Item(name='Grease', unit=y, subgroup=t))

        main = Godown.default()
        dept = sav (Department(name='Intermix'))
        
        iss1 = cl_sav( Issue(date=date.today(), item=i1, quantity=Decimal('1'), source=main, destination=dept))

        iss1.delete()
        
        cl_sav( Issue(date=date.today(), item=i1, quantity=Decimal('1'), source=main, destination=dept))
        
        iss1.activate()


    def test_item_balance(self):
        """Verify item balance, on PurchaseEntry operations"""
        
        sup = sav( mksup('CSK', 'Traders'))
        i = sav( Invoice(number='E109', date=date.today(), supplier=sup))

        y = sav( Unit(name='Milli Liters', symbol='ml', decimal_places=3))
        g = sav( Group(name='Mechanical'))
        t = sav( SubGroup(name='Liquids', group=g))
        
        i1 = sav( Item(name='Oil', unit=y, subgroup=t, opening_stock=Decimal('8.445')))

        u2 = sav( Unit(name='Deci Meter', symbol='dm', decimal_places='1'))
        i2 = sav( Item(name='Wire', unit=u2, opening_stock=Decimal('1.1'), subgroup=t))

        main = Godown.default()
        p1 = sav( PurchaseEntry(item=i1, quantity=Decimal('1.555'), unit_price=Decimal(10), invoice=i))

        self.assertEquals(main.item_balance(i1), 10)
        
