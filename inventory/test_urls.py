import unittest
from inventory.urls import urlpatterns
from django.core.urlresolvers import RegexURLPattern
from inventory import views

def match(view, pattern):
    for url in urlpatterns:
        if url.__class__ is RegexURLPattern and url.callback is view and \
           url.regex.match(pattern):
            return True
    
    return False

# Create your tests here.
class URLVerify(unittest.TestCase):

    def test_URLs(self):
        """Verifies the URLs are present"""

        #home
        self.assertTrue(match(views.home, ''))

        #items
        self.assertTrue(match(views.items, 'items/'))

        #departments
        self.assertTrue(match(views.departments, 'departments/'))

        #item
        self.assertTrue(match(views.item, 'item/1024/'))
        self.assertTrue(match(views.item, 'item/2/do_something/'))

        #department
        self.assertTrue(match(views.department, 'department/1234/'))
        self.assertTrue(match(views.department, 'department/1234/show_detail/'))

        #godown
        self.assertTrue(match(views.godown, 'godown/009/'))

